import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, Resolve } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from '../auth/services/auth.service';
import { User } from '../models/user';
import { UserService } from '../panel/Services/user.service';

@Injectable()
export class UserProfileResolver implements Resolve<User> {
    constructor(
        private userService: UserService,
        private authService: AuthService,
        private alertService: ToastrService,
        private router: Router) { }

    resolve(route: ActivatedRouteSnapshot): Observable<User> {
        return this.userService.getUser(this.authService.decodedToken.nameid).pipe(
            catchError(error => {
                this.alertService.error('خطا در دریافت اطلاعات', 'خطا');
                this.router.navigate(['panel/userinfo/profile']);
                return of(null);
            })
        );
    }
}
