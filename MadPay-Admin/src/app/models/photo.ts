export interface Photo {
    id: string;
    description: string;
    alt: string;
    isMain: string;
    url: string;
}
