import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            catchError(error => {
                // Error
                if (error instanceof HttpErrorResponse) {
                    const appError = error.headers.get('App-Error');
                    if (appError) {
                        return throwError(appError);
                    }
                    // ModelState Error
                    const serverError = error.error.errors;
                    let modelStateError = '';
                    if (modelStateError && typeof serverError === 'object') {
                        for (const key in serverError) {
                            if (serverError[key]) {
                                modelStateError += serverError[key] + '\n';
                            }
                        }
                    }
                    // Customize Error
                    let ErrorExcep = '';
                    const ServerErrorExcep = error.error;
                    if (ServerErrorExcep && typeof ServerErrorExcep === 'object') {
                        if (ServerErrorExcep.status === false) {
                            ErrorExcep = ServerErrorExcep.message;
                        }
                    }
                    return throwError(modelStateError || ErrorExcep || serverError || 'خطایی در سرور رخ داده است');
                }
            })
        );
    }
}
export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
};
