import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanelComponent } from './Panel.component';
import { DashboardComponent } from './Components/components/dashboard/dashboard.component';
import { ProfileComponent } from './Components/components/profile/profile.component';
import { DocumentComponent } from './Components/components/document/document.component';
import { UserProfileResolver } from '../Resolvers/userProfile.Resolver';
import { UnsavedGuard } from '../guards/prevent-unsaved.guard';

const routes: Routes = [
    {
        path: '',
        component: PanelComponent,
        children: [
            { path: 'dashboard', component: DashboardComponent },
            {
                path: 'userinfo/profile', component: ProfileComponent,
                resolve: { users: UserProfileResolver },
                canDeactivate: [UnsavedGuard]
            },
            { path: 'userinfo/documents', component: DocumentComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PanelRoutingModule { }
