import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { environment } from 'src/environments/environment';
// const httpOption: object = {
//   header: new HttpHeaders({
//     Authorization: 'Bearer ' + localStorage.getItem('token')
//   })
// };
@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiUrl + 'Site/panel/users/';

  constructor(private http: HttpClient) { }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.baseUrl);
  }

  getUser(id): Observable<User> {
    return this.http.get<User>(this.baseUrl + id);
  }

  updateUserInfo(id: string, user: User) {
    return this.http.put(this.baseUrl + id, user);
  }
  updateUserPass(id: string, passModel: any) {
    return this.http.put(this.baseUrl + 'ChangeUserPassword/' + id, passModel);
  }
}
