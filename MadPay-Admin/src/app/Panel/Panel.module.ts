import { NgModule } from '@angular/core';
import { PanelComponent } from './Panel.component';
import { PanelRoutingModule } from './panel-routing.module';
import { DashboardComponent } from './Components/components/dashboard/dashboard.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { SidebarComponent } from './Components/sidebar/sidebar.component';
import { ProfileComponent } from './Components/components/profile/profile.component';
import { DocumentComponent } from './Components/components/document/document.component';
import { UserService } from './Services/user.service';
import { UserProfileResolver } from '../Resolvers/userProfile.Resolver';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UnsavedGuard } from '../guards/prevent-unsaved.guard';
import { FileUploadModule } from 'ng2-file-upload';
import { PersianTimeAgoPipe } from 'persian-time-ago-pipe';

@NgModule({
  imports: [
    PanelRoutingModule,
    FormsModule,
    CommonModule,
    FileUploadModule
  ],
  declarations:
    [
      PanelComponent,
      DashboardComponent,
      DocumentComponent,
      NavbarComponent,
      SidebarComponent,
      PersianTimeAgoPipe,
      ProfileComponent
    ],
  providers: [
    UserService,
    UserProfileResolver,
    UnsavedGuard
  ]
})
export class PanelModule { }
