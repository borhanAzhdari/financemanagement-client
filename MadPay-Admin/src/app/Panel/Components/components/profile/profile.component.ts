import { Component, EventEmitter, HostListener, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Photo } from 'src/app/models/photo';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/panel/Services/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  @Output() getUserPhotoUrl = new EventEmitter<string>();
  uploader: FileUploader;
  hasBaseDropZoneOver = false;
  hasAnotherDropZoneOver = false;
  user: User;
  photoUrl: string;
  passModel: any = {};
  baseUrl = environment.apiUrl;
  editForm: FormGroup;
  @HostListener('window:beforeunloaded', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  constructor(
    private userService: UserService,
    private alertService: ToastrService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.authService.currentPhotoUrl.subscribe(pu => this.photoUrl = pu);
    this.loadUser();
    this.initializeUploader();
    this.createEditUserInfoForm();
  }
  createEditUserInfoForm() {
    this.editForm = this.formBuilder.group({
      name: ['', Validators.required],
      userName: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      city: [''],
      address: [''],
      gender: ['', Validators.required]
    });
  }


  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url: this.baseUrl + 'site/admin/' + this.authService.decodedToken.nameid + '/photos',
      authToken: 'Bearer ' + localStorage.getItem('token'),
      allowedFileType: ['image'],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024,
      queueLimit: 1
    });
    this.uploader.onAfterAddingFile = (file) => {
      file.withCredentials = false;
    };
    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        this.alertService.success('عکس پروفایل تغییر کرد', 'موفق');
        const res: Photo = JSON.parse(response);
        // this.user.photoUrl = res.url;
        // this.updateUserPhotoUrl(res.url);
        this.authService.changePhotoUrl(res.url);
        this.authService.currentUser.photoUrl = res.url;
        localStorage.setItem('user', JSON.stringify(this.authService.currentUser));
      }
    };
  }
  updateUserPhotoUrl(photoUrl): Observable<string> {
    return this.user.photoUrl = photoUrl;
  }
  loadUser() {
    // this.route.data.subscribe(data => {
    //   this.user = data.user;
    // });
    this.userService.getUser(this.authService.decodedToken.nameid).subscribe((user: User) => {
      this.user = user;
    }, error => {
      this.alertService.error(error);
    });
  }
  updateUserInfo() {
    this.userService.updateUserInfo(this.authService.decodedToken.nameid, this.user).subscribe(next => {
      this.alertService.success('اطلاعات کاربری با موفقیت وارد شد', 'موفق');
      // this.editForm.form.markAsPristine();
      this.editForm.reset(this.user);
    }, error => {
      this.alertService.error(error, 'خطا');
    });
  }
  updateUserPass() {
    this.userService.updateUserPass(this.authService.decodedToken.nameid, this.passModel).subscribe(next => {
      this.alertService.success('پسورد شما با موفقیت وارد شد', 'موفق');
      this.editForm.reset(this.user);
    }, error => {
      this.alertService.error(error, 'خطا');
    });
  }
}
