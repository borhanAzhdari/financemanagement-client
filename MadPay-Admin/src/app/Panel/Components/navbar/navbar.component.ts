import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/auth/services/auth.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  photoUrl: string;
  jwtHelper = new JwtHelperService();
  constructor(private router: Router, private alertService: ToastrService, public authService: AuthService) { }
  ngOnInit() {
    this.authService.currentPhotoUrl.subscribe(pu => this.photoUrl = pu);
    // this.getDecodedToken();
    this.loadUser();
  }
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    this.authService.decodedToken = null;
    this.authService.currentUser = null;
    this.router.navigate(['/auth/login']);
    this.alertService.warning('خروج با موفقیت انجام شد ', 'موفق');
  }
  // getDecodedToken() {
  //   const token = localStorage.getItem('token');
  //   if (token) {
  //     this.authService.decodedToken = this.jwtHelper.decodeToken(token);
  //   }
  // }
  loadUser() {
    const users: User = JSON.parse(localStorage.getItem('user'));
    if (users) {
      this.authService.currentUser = users;
      this.authService.changePhotoUrl(users.photoUrl);
    }
  }
}
