import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfileComponent } from '../panel/Components/components/profile/profile.component';

@Injectable({ providedIn: 'root' })
export class UnsavedGuard implements CanDeactivate<ProfileComponent> {
    canDeactivate(
        component: ProfileComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        if (component.editForm.dirty) {
            return confirm('شما تغییراتی ایجاد کرده اید با خروج تغییرات ذخیره نمیشوند!');
        }
        return true;
    }
}
