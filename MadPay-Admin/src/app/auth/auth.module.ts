import { NgModule } from '@angular/core';
import { AuthComponent } from './auth.component';
import { BrowserModule } from '@angular/platform-browser';
import { LoginComponent } from '../auth/Components/login/login.component';
import { RegisterComponent } from '../auth/Components/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AuthRoutingModule } from './auth-routing.module';
@NgModule({
  imports: [
    AuthRoutingModule,
    FormsModule ,
    ReactiveFormsModule,
     HttpClientModule
  ],
  declarations: [AuthComponent,
    LoginComponent,
    RegisterComponent],
    providers: [ ],
})
export class AuthModule { }
