import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/models/user';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = environment.apiUrl + 'Site/Panel/auth/';
  JWT = new JwtHelperService();
  decodedToken: any;
  currentUser: User;
  photoUrl = new BehaviorSubject<string>('');
  currentPhotoUrl = this.photoUrl.asObservable();

  constructor(private http: HttpClient) { }
  changePhotoUrl(photoUrl: string)
  {
    this.photoUrl.next(photoUrl);
  }
  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((resp: any) => {
        const userRes = resp;
        if (userRes) {
          localStorage.setItem('token', userRes.token);
          localStorage.setItem('user', JSON.stringify(userRes.user));
          this.currentUser = userRes.user;
          this.decodedToken = this.JWT.decodeToken(userRes.token);
          this.changePhotoUrl(this.currentUser.photoUrl);
        }
      })
    );
  }
  register(model: any) {
    return this.http.post(this.baseUrl + 'register', model);
  }
  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.JWT.isTokenExpired(token);
  }
}
