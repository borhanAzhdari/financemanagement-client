import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import '../../../../assets/vendors/js/chartist.min.js';
import '../../../../assets/js/dashboard-ecommerce.js';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: any = {};
  returnUrl = '';
  constructor(private authService: AuthService, private router: Router, private alertService: ToastrService,
              private route: ActivatedRoute) { }
  ngOnInit() {
    this.model.isremember = true;
    // tslint:disable-next-line:no-string-literal
    this.route.queryParams.subscribe(params => this.returnUrl = params['return'] || '/panel/dashboard');
    if (this.loggedIn()) {
      this.router.navigate([this.returnUrl]);
    }
  }
  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertService.success('موفق', 'ورود با موفقیت انجام شد');
      this.router.navigate([this.returnUrl]);
    }, error => {
      console.log(error);
      this.alertService.error(error, 'خطا');
    });
  }
  loggedIn() {
    return this.authService.loggedIn();
  }
}
