import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  model: any = {};
  registerForm: FormGroup;
  constructor(private authService: AuthService, private alertService: ToastrService) { }

  ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl('', Validators.required),
      username: new FormControl('', [Validators.required, Validators.email]),
      phoneNumber: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(10)]),
      confirmPassword: new FormControl('', Validators.required),
      approveRule: new FormControl(false, Validators.required)
    }, [this.passMatchValidator, this.approveRulesValidator]);
  }
  passMatchValidator(g: FormGroup) {
    return g.get('password').value === g.get('confirmPassword').value ? null : { mismatch: true };
  }
  approveRulesValidator(g: FormGroup) {
    return g.get('approveRule').value === true ? null : {mismatch: true};
  }
  Register() {
    console.log(this.registerForm.value);

    // this.authService.register(this.model).subscribe(() => {
    //   this.alertService.success('موفق', 'ثبت نام با موفقیت انجام شد');
    // }, error => {
    //   this.alertService.error(error, 'خطا در ثبت نام');
    // });
  }
}
