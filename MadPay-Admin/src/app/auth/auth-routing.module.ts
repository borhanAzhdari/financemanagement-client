import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Components/login/login.component';
import { AuthComponent } from './auth.component';
import { RegisterComponent } from './Components/register/register.component';
import { NgModuleFactory } from '@angular/core/src/r3_symbols';

const routes: Routes = [
    {
        path: '',
        component: AuthComponent,
        // tslint:disable-next-line:align
        children: [
            { path: '', component: LoginComponent },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AuthRoutingModule {}
